type ThenCallback<A, B> = (value: A) => B
type WhenCallback<A> = (value: A) => boolean

export class MatchError extends Error {
    constructor(message: string) {
        super(message)

        /* istanbul ignore next */
        if (Error.captureStackTrace) {
            Error.captureStackTrace(this, MatchError)
        }

        this.name = 'MatchError'
    }
}

export interface Match<A, B> {
    readonly get: () => B
    readonly when: (callback: WhenCallback<A>) => {
        readonly then: (callback: ThenCallback<A, B>) => Match<A, B>
    }
    readonly default: (callback: ThenCallback<A, B>) => Match<A, B>
}

export type MatchFn = <A, B>(value: A) => Match<A, B>

interface WhenThen<A, B> {
    readonly when: WhenCallback<A>
    readonly then: ThenCallback<A, B>
}

function unboundMatch<A, B>(value: A): Match<A, B> {
    const patterns: Array<WhenThen<A, B>> = []
    let def: ThenCallback<A, B> | undefined = undefined

    const when = function (validationCallback: WhenCallback<A>) {
        const then = function (thenCallback: ThenCallback<A, B>) {
            patterns.push({
                when: validationCallback,
                then: thenCallback
            })
            return this
        }.bind(this)

        return {
            then
        }
    }
    const defaultMatch = function (callback: ThenCallback<A, B>) {
        def = callback

        return this
    }

    const response = {
        get: () => {
            const pattern = patterns.find(pattern => pattern.when(value))
            if (pattern) {
                return pattern.then(value)
            } else {
                if (def) {
                    return def(value)
                } else {
                    throw new MatchError(`Nothing matched for value: ${value}`)
                }
            }
        },
        when: undefined,
        default: undefined
    }

    response.when = when.bind(response)
    response.default = defaultMatch.bind(response)
    return Object.freeze(response) as any as Match<A, B>
}

export const match: MatchFn  = unboundMatch.bind(unboundMatch)
