import {match, MatchError} from '../src/match'

describe('MatchError', () => {
    it('should instantiate a MatchError correctly', () => {
        const error = new MatchError('KAPOW')

        expect(error.name).toEqual('MatchError')
        expect(error.message).toEqual('KAPOW')
    })
})

describe('match', () => {
    const expectations = [
        {
            value: 'bye',
            expectation: 'MEANT TO SAY GOODBYE'
        },
        {
            value: 'hello',
            expectation: 1
        },
        {
            value: 'unexpected',
            expectation: 'DEFAULT VALUE'
        }
    ]

    expectations.forEach(expectation => {
        it(`should match ${expectation.value} and return the expected value`, () => {

            const result: string | number = match<string, string | number>(expectation.value)
                .when(_ => _ === 'bye').then(_ => 'MEANT TO SAY GOODBYE')
                .when(_ => _ === 'hello').then(_ => 1)
                .default(_ => 'DEFAULT VALUE')
                .get()

            expect(result).toStrictEqual(expectation.expectation)
        })
    })

    it('throw an error if there is no match and no default defined', () => {
        const value = 'unexpected'
        const lazyMatch = match(value)
            .when(_ => _ === 'bye').then(_ => 'MEANT TO SAY GOODBYE')
            .when(_ => _ === 'hello').then(_ => _.toUpperCase())

        expect(() => lazyMatch.get()).toThrow(`Nothing matched for value: ${value}`)
    })
})
